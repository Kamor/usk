<?php

namespace kamilmusial\Bundle\USKBundle\Controller;

use kamilmusial\Bundle\USKBundle\Exception\EndOfWordsException;
use kamilmusial\Bundle\USKBundle\Exception\SameSourceTargetException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class QuestionController
 * @package kamilmusial\Bundle\USKBundle\Controller
 */
class QuestionController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('USKBundle::index.html.twig', [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @param $source
     * @param $target
     * @param Request $request
     * @return Response
     * @throws SameSourceTargetException
     */
    public function showAction($source, $target, Request $request)
    {
        if ($source == $target) {
            throw new SameSourceTargetException('Source and target language are the same.');
        }

        $done = $request->getSession()->get('done');

        try {
            $items = $this->getDoctrine()
                ->getRepository('USKBundle:Word')
                ->getRandom($source, $target, $done);
        } catch (EndOfWordsException $e) {
            $response = $this->render('USKBundle::end.html.twig', [
                'points' => $request->getSession()->get('points', 0),
                'questionCount' => $request->getSession()->get('questionCount', 0),
                'percent' => $request->getSession()->get('questionCount', 1)
                    ? $request->getSession()->get('points', 0) / $request->getSession()->get('questionCount', 1) * 100
                    : 0,
            ]);

            $this->clearPoints($request);

            return $response;
        }

        $done[] = $items['word']->getWordId();
        $request->getSession()->set('done', $done);

        return $this->render('USKBundle::quiz.html.twig', [
            'items' => $items,
            'points' => $request->getSession()->get('points', 0),
            'questionCount' => $request->getSession()->get('questionCount', 0),
            'target' => $target,
        ]);
    }

    /**
     * @param $lang
     * @param Request $request
     * @return Response
     */
    public function answerAction($lang, Request $request)
    {
        $word1 = $request->request->get('word1');
        $word2 = $request->request->get('word2');

        $check = $this->getDoctrine()->getRepository('USKBundle:Word')->check($word1, $word2);

        $points = $request->getSession()->get('points', 0);
        if ($check) {
            $request->getSession()->set('points', ++$points);
        }
        $questionCount = $request->getSession()->get('questionCount', 0);
        $request->getSession()->set('questionCount', ++$questionCount);

        return new Response(
            json_encode([
                'check' => $check,
                'points' => $points,
                'questionCount' => $questionCount,
                'correct' => $this->getDoctrine()->getRepository('USKBundle:Word')->getCorrect($word1, $lang)
            ]),
            200,
            ['Content-Type' => 'application/json']
        );
    }

    /**
     * @param Request $request
     */
    public function clearPoints(Request $request)
    {
        $request->getSession()->set('points', 0);
        $request->getSession()->set('questionCount', 0);
        $request->getSession()->set('done', null);
    }

}
