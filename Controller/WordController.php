<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 22.01.2016
 * Time: 21:07
 */

namespace kamilmusial\Bundle\USKBundle\Controller;


use kamilmusial\Bundle\USKBundle\Entity\Word;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WordController extends Controller
{

    public function listAction($lang1, $lang2, $category)
    {
        $doctrine = $this->getDoctrine();

        list ($lang1, $lang2) = $doctrine->getRepository('USKBundle:Lang')->findBy(['id' => [$lang1, $lang2]]);

        return $this->render('USKBundle:panel:words.html.twig', [
            'words' => $doctrine->getRepository('USKBundle:Word')
                ->getList([$lang1->getAbbr() => $lang1->getId(), $lang2->getAbbr() => $lang2->getId()], $category),
            'langs' => [$lang1, $lang2],
            'category' => $doctrine->getRepository('USKBundle:Category')->find($category)
        ]);
    }

    public function addAction($lang1, $lang2, $category, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $word1 = $request->request->get('word1');
        $word2 = $request->request->get('word2');

        $maxWordId = $this->getDoctrine()->getRepository('USKBundle:Word')->getMaxWordId();
        $wordId = reset($maxWordId) + 1;

        $word = new Word();
        $word->setWordId($wordId)
            ->setWord($word1)
            ->setLang($lang1)
            ->setCategory($category)
            ->setCdate(new \DateTime());

        $em->persist($word);
        $em->flush();

        $word = new Word();
        $word->setWordId($wordId)
            ->setWord($word2)
            ->setLang($lang2)
            ->setCategory($category)
            ->setCdate(new \DateTime());

        $em->persist($word);
        $em->flush();

        return new Response(json_encode($word->getId()));

    }
}
