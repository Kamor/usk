<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 22.01.2016
 * Time: 21:07
 */

namespace kamilmusial\Bundle\USKBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PanelController extends Controller
{

    public function categoriesAction()
    {
        return $this->render('USKBundle:panel:category.html.twig', [
            'categories' => $this->getDoctrine()->getRepository('USKBundle:Category')->findAll(),
        ]);
    }
}
