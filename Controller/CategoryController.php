<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 22.01.2016
 * Time: 21:07
 */

namespace kamilmusial\Bundle\USKBundle\Controller;


use kamilmusial\Bundle\USKBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{

    public function listAction()
    {
        return $this->render('USKBundle:panel:category.html.twig', [
            'categories' => $this->getDoctrine()->getRepository('USKBundle:Category')->findAll(),
        ]);
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $name = $request->request->get('name');

        $category = new Category();
        $category->setName($name)
            ->setCdate(new \DateTime());

        $em->persist($category);
        $em->flush();

        return new Response(json_encode($category->getId()));

    }
}
