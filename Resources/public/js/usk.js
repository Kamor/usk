var USK = USK || {};

USK.Main = USK.Main || {
        send : function(word1, word2) {
            var lang = $('.question').data('lang');

            $.ajax({
                method: "POST",
                url: '/answer/' + lang,
                data: {word1: word1, word2: word2}
            }).done(function(data){
                if (data.check) {
                    USK.Main.markCorrect();
                } else {
                    USK.Main.markIncorrect();
                    $('[data-word="' + data.correct + '"]').addClass('btn-success');
                }
                $('.btn-active').removeClass('btn-active');

                $('.points').html(data.points + '/' + data.questionCount);
            });
        },

        hookEvents : function() {
            $('.question').on('click', '.word', function(e){
                if ($(this).is('[disabled]')) {
                    return;
                }
                e.preventDefault();

                var word1 = $('.word1 h1').data('word'),
                    word2 = $(this).data('word');

                $(this).addClass('btn-active');
                $('.word').attr('disabled', 'disabled');
                USK.Main.send(word1, word2);
            }).on('click', '.word1', function (e) {
                e.preventDefault();

                if(!$('.btn-success').length && !$('.end').length) {
                    return;
                }

                location.reload();
            });

            $('.words').on('click', '#addWord', function(e){
                USK.Main.addWord();
            });

            $('.categories').on('click', '#addCategory', function(e){
                USK.Main.addCategory(e);
            });
        },

        addWord : function() {
            var lang1 = $('.word1').data('lang1'),
                lang2 = $('.word2').data('lang2'),
                word1 = $('.word1').val(),
                word2 = $('.word2').val(),
                category = $('.category').data('category');

            if ( ! word1.length || ! word2.length){
                return;
            }

            $.ajax({
                method: "POST",
                url: '/panel/category/add/' + lang1 +'/' + lang2 + '/' + category,
                data: {word1: word1, word2: word2}
            }).done(function(data){
                console.log(data);
                $('.word1').val('');
                $('.word2').val('');
                location.reload();
            });
        },

        addCategory : function(e) {
            e.preventDefault();

            var category = $('#category').val();

            $.ajax({
                method: "POST",
                url: '/panel/categories/add',
                data: {name: category}
            }).done(function(data){
                console.log(data);
                location.reload();
            });
        },

        markCorrect : function() {
            $('.btn-active').addClass('btn-success');
        },

        markIncorrect : function () {
            $('.btn-active').addClass('btn-danger');
        }
    };

$(document).ready(function () {
    USK.Main.hookEvents();
});
