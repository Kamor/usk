<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 20.01.2016
 * Time: 23:31
 */

namespace kamilmusial\Bundle\USKBundle\Entity;


use kamilmusial\Bundle\USKBundle\Exception\EndOfWordsException;
use kamilmusial\Bundle\USKBundle\Exception\WordCountFailedException;
use Doctrine\ORM\EntityRepository;

class WordRepository extends EntityRepository
{
    public function getRandom($source, $target, $done = null)
    {
        $words = $this->createQueryBuilder('w')
            ->addSelect('RAND() as HIDDEN rand')
            ->orderBy('rand')
            ->setMaxResults(4)
            ->andWhere('w.lang = :lang');

        if ( ! empty($done)) {
            $words = $words->andWhere('w.wordId NOT IN (:done)')
                ->setParameter('done', $done);
        }
        $words = $words->setParameter('lang', $source)
            ->getQuery()
            ->getResult();

        if (count($words) < 4) {
            throw new EndOfWordsException();
        }

        $targets = [];
        foreach ($words as $word) {
            $targets[] = $word->getWordId();
        }

        $translations = $this->createQueryBuilder('w')
            ->addSelect('RAND() as HIDDEN rand')
            ->orderBy('rand')
            ->andWhere('w.lang = :lang')
            ->andWhere('w.wordId IN (:targets)')
            ->setParameters(['lang' => $target, 'targets' => $targets])
            ->getQuery()
            ->getResult();

        return ['word'=> $words[0], 'translations' => $translations];

    }

    public function check($word1, $word2)
    {
        $check = $this->createQueryBuilder('w')
            ->select('w')
            ->orWhere('w.word = :word1')
            ->orWhere('w.word = :word2')
            ->setParameters(['word1' => $word1, 'word2' => $word2])
            ->getQuery()
            ->getResult();

        if (count($check) != 2) {
            throw new WordCountFailedException('Word translation not found (' . $word1 . ', ' . $word2 . ')');
        }

        return $check[0]->getWordId() == $check[1]->getWordId();
    }

    public function getCorrect($word, $lang)
    {
        $target = $this->createQueryBuilder('w')
            ->select('w.wordId')
            ->andWhere('w.word = :word')
            ->setParameters(['word' => $word])
            ->getQuery()
            ->getResult();

        $source = $this->createQueryBuilder('w')
            ->select('w')
            ->andWhere('w.wordId = :wordId')
            ->andWhere('w.lang = :lang')
            ->setParameters(['wordId' => $target[0]['wordId'], 'lang' => $lang])
            ->getQuery()
            ->getResult();

        return $source[0]->getWord();
    }

    public function getList(array $langs, $category)
    {
        $sql = 'SELECT word_id as wordId,';

        foreach ($langs as $lang => $id) {
            $sql .= 'MAX(CASE WHEN lang = ' . $id . ' THEN word END) `' . $lang . '`, ';
        }

        $sql = rtrim($sql, ', ');

        $sql .= 'FROM words
         WHERE lang IN(' . implode(',', $langs) . ')
         AND category = ' . $category . '
         GROUP BY word_id';

        $words = $this->getEntityManager()->getConnection()->prepare($sql);
        $words->execute();
        return $words->fetchAll();
    }

    public function getMaxWordID()
    {
        $max = $this->createQueryBuilder('w')
            ->select('MAX(w.wordId)')
            ->getQuery()
            ->getResult();
        return array_shift($max);
    }
}
