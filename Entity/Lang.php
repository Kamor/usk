<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 20.01.2016
 * Time: 21:59
 */

namespace kamilmusial\Bundle\USKBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="kamilmusial\Bundle\USKBundle\Entity\WordRepository");
 * @ORM\Table(name="languages")
 */
class Lang
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $abbr;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $lang;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $cdate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set abbr
     *
     * @param string $abbr
     *
     * @return Lang
     */
    public function setAbbr($abbr)
    {
        $this->abbr = $abbr;

        return $this;
    }

    /**
     * Get abbr
     *
     * @return string
     */
    public function getAbbr()
    {
        return $this->abbr;
    }

    /**
     * Set lang
     *
     * @param \lang $lang
     *
     * @return Lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return \lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set cdate
     *
     * @param \DateTime $cdate
     *
     * @return Lang
     */
    public function setCdate($cdate)
    {
        $this->cdate = new \DateTime();

        return $this;
    }

    /**
     * Get cdate
     *
     * @return \DateTime
     */
    public function getCdate()
    {
        return $this->cdate;
    }
}
