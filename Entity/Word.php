<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 20.01.2016
 * Time: 21:59
 */

namespace kamilmusial\Bundle\USKBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="kamilmusial\Bundle\USKBundle\Entity\WordRepository");
 * @ORM\Table(name="words")
 */
class Word
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", name="word_id")
     */
    protected $wordId;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $word;

    /**
     * @ORM\Column(type="integer")
     */
    protected $lang;

    /**
     * @ORM\Column(type="integer")
     */
    protected $category;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $cdate;

    /**
     * Set abbr
     *
     * @param string $abbr
     *
     * @return Word
     */
    public function setAbbr($abbr)
    {
        $this->abbr = $abbr;

        return $this;
    }

    /**
     * Get abbr
     *
     * @return string
     */
    public function getAbbr()
    {
        return $this->abbr;
    }

    /**
     * Set lang
     *
     * @param integer $lang
     *
     * @return Word
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return integer
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wordId
     *
     * @param integer $wordId
     *
     * @return Word
     */
    public function setWordId($wordId)
    {
        $this->wordId = $wordId;

        return $this;
    }

    /**
     * Get wordId
     *
     * @return integer
     */
    public function getWordId()
    {
        return $this->wordId;
    }

    /**
     * Set word
     *
     * @param string $word
     *
     * @return Word
     */
    public function setWord($word)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word
     *
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return Word
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set cdate
     *
     * @param \DateTime $cdate
     *
     * @return Word
     */
    public function setCdate($cdate)
    {
        $this->cdate = new \DateTime();

        return $this;
    }

    /**
     * Get cdate
     *
     * @return \DateTime
     */
    public function getCdate()
    {
        return $this->cdate;
    }
}
